tool
extends Sprite

export var paddle_size = Vector2(100, 20) setget size

var y = null

func _ready():
	y = position.y

func size(new_size):
	paddle_size = new_size
	update()

func _physics_process(delta):
	position.y = y
	update()

func _draw():
	var inv = get_global_transform().affine_inverse()
	draw_set_transform(inv.get_origin(), inv.get_rotation(), inv.get_scale())
	
	var rect = Rect2(global_position, paddle_size)
	
	draw_rect(rect, Color(0, 0, 1, 1))
